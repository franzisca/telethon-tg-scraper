# telethon - https://github.com/LonamiWebs/Telethon
# telethon api - https://docs.telethon.dev/en/latest/quick-references/client-reference.html#client-ref
# iter_messages - https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.iter_messages
from telethon import TelegramClient, events, sync
from telethon.tl.types import InputMessagesFilterUrl, MessageEntityUrl, MessageEntityTextUrl
import telethon
import re
from pprint import pprint

client = None
convs_all = None
conv_last = None
my_id = None

def attach_client(client_income):
    global client
    client = client_income
    load_my_id()
    load_convs()

def load_convs():
    global convs_all
    convs_all = client.get_dialogs()
    print('Chats cargados')

def load_my_id():
    global my_id
    my_id = client.get_me().id
    print('Id de usuarie cargado')

NAME_MAX = 50
def print_conv(i, c):
    name_str = len(c.name) > NAME_MAX and f'{c.name[:NAME_MAX]}...' or c.name.ljust(NAME_MAX)
    date_str = c.date.strftime('%Y-%m-%d %H:%M%Z')

    username = None
    if c.entity:
        try:
            username = c.entity.username
        except AttributeError:
            pass
    username_str = username and f'@{username}' or ''

    #print(c.stringify())
    print(str(i+1).rjust(3,'0'), name_str, date_str, username_str or c.entity.id)

# listar mis chats
def list_chats(top):
    if top:
        convs = convs_all[:top]
    else:
        convs = convs_all

    for i, c in enumerate(convs):
        print_conv(i, c)

# buscar chats por nombre
def search_chats(text):
    text = text.lower() #TODO VACIO

    found_conv = False
    for i, c in enumerate(convs_all):
        if c.name.lower().find(text) != -1:
            found_conv = True
            print_conv(i, c)

    if not found_conv:
        print('Sin resultados de búsqueda')

# buscar chats por nombre
def get_msgs(conv_i, top=20):
    conv = convs_all[int(conv_i)-1]

    print(f'Leyendo chat: {conv.name}...')
    msgs = client.get_messages(conv.id, limit=top)

    USERNAME_MAX = 9
    MSG_MAX = 60
    for m in msgs:
        #print(m)
        #print(vars(m._sender))

        username = m._sender.username
        #print(type(m._sender))
        if username:
            username = len(username) > USERNAME_MAX and f'{username[:USERNAME_MAX]}..' or username.ljust(USERNAME_MAX)
        user_str = username and f'@{username}' or '<sin arroba>'

        if isinstance(m._sender, telethon.tl.types.User):
            first_name = m._sender.first_name or '""'
            last_name = m._sender.last_name or '""'
        else:
            first_name = '""'
            last_name = '""'
        full_name_str = f'{first_name} {last_name}'

        msg = m.message
        if msg.find('\n') != -1:
            msg_lines = m.message.split('\n')
            msg = f'{msg_lines[0]}...'
        msg_str = len(msg) > MSG_MAX and f'{msg[:MSG_MAX]}...' or msg.ljust(MSG_MAX)

        #print(f'[{user_str}] {full_name_str}: {msg_str}')
        print('[{0:12}] {1}: {2}'.format(user_str, full_name_str, msg_str))

# descargar todos los mensajes de un chat a archivos
DUMP_FOLDER = 'dump-msgs'
def download_all_msgs(conv_i):
    conv = convs_all[int(conv_i)-1]

    print(f'Leyendo chat: {conv.name}...')
    msgs_iter = client.iter_messages(conv.id)

    print(f'Descargando mensajes: ', end='', flush=True)
    msgs_count = 0

    for m in msgs_iter:
        m_date_str = m.date.strftime('%Y-%m-%d')
        m_file_name = f'rl-cotd-{m_date_str}-{m.id}.md'

        if not m.text: continue

        open(f'{DUMP_FOLDER}/{m_file_name}', 'w+').write(m.text)
        msgs_count += 1
        print('.', end='', flush=True)

    print()
    print(f'{msgs_count} mensajes descargados a carpeta {DUMP_FOLDER}')

REG_IP = re.compile('^\d+(?:\.\d+)+(?::\d+)?$')
def print_msg_urls(m):
    urls = get_msg_urls(m)
    for u in urls:
        print(u)
def get_msg_urls(m):
    # cada mensaje es de clase Message: https://docs.telethon.dev/en/latest/modules/custom.html#telethon.tl.custom.message.Message
    urls = []
    if len(m.entities) == 0:
      print('Salteando mensaje sin entities')
    else:
      for e in m.entities:
        # tipos de entities: https://tl.telethon.dev/types/message_entity.html
        if isinstance(e, MessageEntityTextUrl):
          url = e.url
        elif isinstance(e, MessageEntityUrl):
          url = m.message[e.offset:e.offset+e.length]
        else:
          # hashtags, código, menciones, bold, phone, etc.
          #print(f'Salteando entity {e}')
          continue

        if not url:
          print(f'Salteando link vacío')
          continue

        if REG_IP.match(url):
          print(f'Salteando link a IP')
          continue

        urls.append(url)
      #end for
    #end if
    return urls


def get_links(conv_i, top=20):
    conv = convs_all[int(conv_i)-1]

    # get_messages - https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.get_messages
    # devuelve de clase TotalList: https://docs.telethon.dev/en/latest/modules/helpers.html#telethon.helpers.TotalList
    # filtros - https://tl.telethon.dev/types/messages_filter.html
    msgs = client.get_messages(conv.id, limit=top, filter=InputMessagesFilterUrl)

    if len(msgs) == 0:
      print(f'No se encontró ningún mensaje con link')
      return
    print(f'{len(msgs)} mensajes con links encontrados:')

    for m in msgs:
        print_msg_urls(m)

def get_conv_from_entity_id(entity_id):
    entity_id = int(entity_id)
    for c in convs_all:
        if c.entity and c.entity.id == entity_id:
            return c
    raise Exception(f'Conversación no encontrada con entity id {entity_id}')

def get_conv_from_username(username):
    username = username.lower()
    if username[0] != '@':
        username = f'@{username}'
    for c in convs_all:
        if c.entity:
            try:
                c_username = c.entity.username
                if username == c_username:
                    return c
            except AttributeError:
                pass
    raise Exception(f'Conversación no encontrada con username {username}')

REG_ENTITY_ID = re.compile('^-?[1-9][0-9]{5,}$')
def search_links(conv_id, text, top=20):
    text = text.lower()

    if conv_id[0] == '@':
        conv = get_conv_from_username(conv_id)
    elif REG_ENTITY_ID.match(conv_id):
        conv = get_conv_from_entity_id(conv_id)
    else:
        conv = convs_all[int(conv_id)-1]
    #print(conv.stringify())

    # devuelve de clase TotalList: https://docs.telethon.dev/en/latest/modules/helpers.html#telethon.helpers.TotalList
    msgs = client.iter_messages(conv.id, filter=InputMessagesFilterUrl)

    print(f'- Buscando links con "{text}" en {conv.name}...')

    found = 0
    for m in msgs:
        urls = get_msg_urls(m)
        for u in urls:
            if u.lower().find(text) != -1:
                print(u)
                found+=1
                if found > top:
                    print(f'Máximos resultados alcanzado ({top})')
                    return

    if not found:
        print('Sin resultados')
    else:
        print(f'Encontrados {found} links')


# conseguir la entity de un chat
#convs = client.get_dialogs()
#CHAT_ID = 1331824176
#CHAT_ENTITY = None
#for c in convs:
#	if c.entity.id == CHAT_ID:
#		CHAT_ENTITY = c.entity
#		break;
#CHAT_URL=CHAT_ENTITY

#CHAT_URL='https://t.me/xxx'

# borrar mis últimos 5 msgs del canal
'''mi_id = client.get_me().id
print(mi_id)
mis_msgs = []
messages = client.iter_messages(CHAT_URL, from_user=mi_id)
for _ in range(3):
	m = next(messages)
	mis_msgs.append(m.id)

print(mis_msgs)
client.delete_messages(CHAT_URL, mis_msgs, revoke=True)'''


# mostrar ocurrencias de palabras
'''def print_msgs_count(search):
  # https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.get_messages
	msgs = client.get_messages(CHAT_URL, 0, search=search)
	print(search, msgs.total)
[print_msgs_count(s) for s in ['patricia', 'pato', 'bullrich','bulrich']]'''

# buscar links
'''import asyncio
import re
from telethon.tl.types import InputMessagesFilterUrl, MessageEntityUrl, MessageEntityTextUrl
async def print_msgs_count():
  # get_messages: https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.get_messages
  # filtros: https://tl.telethon.dev/types/messages_filter.html
  msgs = await client.get_messages(CHAT_URL, 225, filter=InputMessagesFilterUrl)
  # devuelve de clase TotalList: https://docs.telethon.dev/en/latest/modules/helpers.html#telethon.helpers.TotalList
  error_malaentity = 0
  reg_ip = re.compile('^\d+(?:\.\d+)+(?::\d+)?$')
  reg_cleanurl = re.compile('^[\w-]+(?:[\.][\w/-]+)+$', re.IGNORECASE)
  reg_emojicode = re.compile('&#\d{6,};')
  urls = []
  urls_nohttp = []
  if len(msgs) == 0:
    print(f'No se encontró nada dentro de los {msgs.total} mensajes')
  else:
    print(f'{len(msgs)} mensajes con links encontrados')
    for m in msgs:
      # cada mensaje es de clase Message: https://docs.telethon.dev/en/latest/modules/custom.html#telethon.tl.custom.message.Message
      if len(m.entities) == 0:
        print('Sin entities, salteando')
        continue
      else:
        for e in m.entities:
          # tipos de entities: https://tl.telethon.dev/types/message_entity.html
          if isinstance(e, MessageEntityTextUrl):
            url = e.url
          elif isinstance(e, MessageEntityUrl):
            url = m.message[e.offset:e.offset+e.length]
          else:
            # hashtags, código, menciones, bold, phone, etc.
            #print(f'Salteando entity {e}')
            #error_malaentity += 1
            continue
          if not url:
            continue
          if reg_ip.match(url):
            continue
          if url.startswith('http'):
            urls.append(url)
            continue
          if reg_cleanurl.match(url):
            urls_nohttp.append(url)
            continue
          # encode: https://www.w3schools.com/python/ref_string_encode.asp
          mascii = m.message[:e.offset].encode('ascii', errors="xmlcharrefreplace").decode('ascii')
          nemojis = len(reg_emojicode.findall(mascii))
          url = m.message[e.offset-nemojis:e.offset-nemojis+e.length]
          if url.startswith('http'):
            urls.append(url)
            continue
          if reg_cleanurl.match(url):
            urls_nohttp.append(url)
            continue
          #print(nemojis)
          #print(mascii)
          print(f'Mala url: {url}')
        #endfor entities
      #return
    print(f'Se encontraron {len(urls)} urls')

    with open('dump.txt', 'w+') as file:
      file.write('\n'.join(urls))
      file.write('\n'.join(urls_nohttp))
    #endfor msgs
#print_msgs_count()
loop = asyncio.get_event_loop()
loop.run_until_complete(print_msgs_count())'''

# ver búsqueda de msj con su contexto
'''def print_msg_ctxt(search, ctxt_n):
	msg = client.get_messages(CHAT_URL, 1, search=search, reverse=True)
	msg_id = msg[0].id
	msgs = client.get_messages(CHAT_URL, min_id=msg_id-1-ctxt_n, max_id=msg_id+ctxt_n)
	[print(m.message) for m in msgs]
print_msg_ctxt('pato',3)'''


# ver últimos 5 msjs
'''usus = {}
messages = client.iter_messages(CHAT_URL)
for _ in range(5):
	m = next(messages)
	usuid = m.from_id
	if usuid in usus: usuname = usus[usuid]
	else: usuname = usus[usuid] = client.get_entity(usuid).username
	dispdate = m.date.strftime('%Y-%m-%d %H:%M%Z')
	print(dispdate, '|', usuname, '|', m.message)'''


### Primeros códigos testeados y andando
# tira stats sobre mi usu
#print(client.get_me().stringify())

# manda msj, no hizo falta el @
#client.send_message('usuarie', 'Hello! Talking to you from Telethon')

# se descarga mi foto
#client.download_profile_photo('me')

# se descarga un gif/imagen/archivo
#messages = client.get_messages('usuarie')
#messages[0].download_media()

# si me mandan un msj con ese pattern yo respondo (semibot)
#@client.on(events.NewMessage(pattern='(?i)hi|hello'))
#async def handler(event):
#    await event.respond('Hey!')
#print('Esperando msjs...')
#client.run_until_disconnected()
