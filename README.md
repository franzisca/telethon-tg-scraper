```
# crear venv, activar e instalar dependencias
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# descomentar y modificar el código deseado dentro de scraper.py

# correr script
API_ID=XX API_HASH=NN python3 scraper.py

# la primera vez que corra va a pedir:
# número de teléfono + código de confirmación [+ contraseña de 2FA si tenés]
```

## comandos usados para obtener links de youtube o invidious
```
cat dump-sound.txt| grep -E 'youtu|invidi' > dump-sound-yt-invi.txt
# corregir "https://" incompletos
sed -i -E 's@^t?t?p?s://@https://@' dump-sound-yt-invi.txt
```
