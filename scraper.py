# telethon - https://github.com/LonamiWebs/Telethon
# telethon api - https://docs.telethon.dev/en/latest/quick-references/client-reference.html#client-ref
# iter_messages - https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.iter_messages
from telethon import TelegramClient, events, sync
from os import environ as env
from sys import exit
import scraper_functions as funcs

# generadas en https://my.telegram.org > API development tools
# se pueden pasar haciendo: ~$ API_ID=NN API_HASH=XX python3 scraper.py
# o setteándolas antes en la shell: ~$ export API_ID=NN API_HASH=XX
# en la primer corrida te va a pedir que verifiques con tu número de celu
api_id, api_hash = env['API_ID'], env['API_HASH']

client = TelegramClient('session_1', api_id, api_hash)
print('Arrancando bot...', end='')
client.start()
print(' hecho')

### Primeros códigos testeados y andando
# tira stats sobre mi usu
user_me = client.get_me()
print(f'Operando con usuarie: #{user_me.id} +{user_me.phone[:4]}... @{user_me.username} "{user_me.first_name}","{user_me.last_name}"')
#print(vars(client.get_me()))
#print(client.get_me().stringify())

# inicializar librería de funciones
funcs.attach_client(client)

# listar los X chats más recientes
#funcs.list_chats(15)

# listar todos los chats
#funcs.list_chats()

# buscar chats por nombre
funcs.search_chats('rl')
#funcs.search_chats('cdp')
#funcs.search_chats('admin')

# del chat número X traer los Y últimos mensajes
funcs.get_msgs('071', top=2)

# descargar todos los mensajes de un chat a archivos
#funcs.download_all_msgs('071')

#funcs.get_links('002', top=1000)

#funcs.search_links('21', 'you', top=6)
#funcs.search_links('-1001290070233', search, top=6)
#search = 'hedge'
#funcs.search_links('1290070233', search, top=6)
#funcs.search_links('1468527486', search, top=6)
#funcs.search_links('376289493', search, top=6)
#funcs.search_links('481115408', search, top=6)


# manda msj, no hizo falta el @
#client.send_message('usuarie', 'Hello! Talking to you from Telethon')

# se descarga mi foto
#client.download_profile_photo('me')

# se descarga un gif/imagen/archivo
#messages = client.get_messages('usuarie')
#messages[0].download_media()

# si me mandan un msj con ese pattern yo respondo (semibot)
#@client.on(events.NewMessage(pattern='(?i)hi|hello'))
#async def handler(event):
#    await event.respond('Hey!')
#print('Esperando msjs...')
#client.run_until_disconnected()
